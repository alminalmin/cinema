-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 26, 2017 at 01:42 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cinema`
--

-- --------------------------------------------------------

--
-- Table structure for table `cinema_hall`
--

CREATE TABLE `cinema_hall` (
  `id_hall` int(11) UNSIGNED NOT NULL,
  `name_hall` varchar(128) NOT NULL,
  `seats_number` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cinema_hall`
--

INSERT INTO `cinema_hall` (`id_hall`, `name_hall`, `seats_number`) VALUES
(1, 'hall_1', 100),
(2, 'hall_2', 60),
(3, 'hall_3', 130),
(4, 'hall_4', 140),
(5, 'hall_5', 30);

-- --------------------------------------------------------

--
-- Table structure for table `movie`
--

CREATE TABLE `movie` (
  `id_movie` int(11) UNSIGNED NOT NULL,
  `name` varchar(45) NOT NULL,
  `duration` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `movie`
--

INSERT INTO `movie` (`id_movie`, `name`, `duration`) VALUES
(1, 'Alien: Covenant', 122),
(2, 'Guardians of the Galaxy ', 137),
(3, 'King Arthur: Legend of the Sword', 126),
(4, 'How to be a Latin Lover', 115),
(5, 'The Boss Baby', 98),
(6, ' Deep', 92);

-- --------------------------------------------------------

--
-- Table structure for table `permision`
--

CREATE TABLE `permision` (
  `permision_id` int(128) NOT NULL,
  `permision_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permision`
--

INSERT INTO `permision` (`permision_id`, `permision_name`) VALUES
(1, 'administrator'),
(2, 'korisnik');

-- --------------------------------------------------------

--
-- Table structure for table `projection`
--

CREATE TABLE `projection` (
  `id_projection` int(11) UNSIGNED NOT NULL,
  `id_hall` int(11) UNSIGNED NOT NULL,
  `id_movie` int(11) UNSIGNED NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `projection`
--

INSERT INTO `projection` (`id_projection`, `id_hall`, `id_movie`, `date`) VALUES
(1, 5, 1, '2017-07-19'),
(2, 4, 3, '2017-07-19'),
(3, 1, 2, '2017-07-20'),
(4, 2, 5, '2017-07-20'),
(5, 3, 4, '2017-07-18'),
(6, 4, 6, '2017-07-18'),
(7, 5, 5, '2017-07-18'),
(8, 2, 4, '2017-07-21'),
(9, 3, 5, '2017-07-21'),
(10, 5, 1, '2017-07-21'),
(11, 1, 3, '2017-07-21'),
(13, 4, 6, '2017-07-21'),
(14, 2, 1, '2017-07-22'),
(15, 1, 2, '2017-07-22'),
(16, 3, 5, '2017-07-22'),
(17, 4, 6, '2017-07-22'),
(18, 2, 6, '2017-07-23'),
(19, 3, 5, '2017-07-23'),
(20, 1, 4, '2017-07-23'),
(21, 5, 6, '2017-07-23'),
(22, 1, 1, '2017-07-24'),
(23, 2, 6, '2017-07-24'),
(24, 4, 3, '2017-07-24'),
(25, 3, 5, '2017-07-24'),
(26, 5, 4, '2017-07-24'),
(27, 2, 5, '2017-07-25'),
(28, 3, 1, '2017-07-25'),
(29, 4, 2, '2017-07-25'),
(30, 5, 6, '2017-07-25');

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

CREATE TABLE `reservation` (
  `id_reservation` int(128) NOT NULL,
  `id_user` int(128) NOT NULL,
  `id_projection` int(128) NOT NULL,
  `Date_reservation` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservation`
--

INSERT INTO `reservation` (`id_reservation`, `id_user`, `id_projection`, `Date_reservation`) VALUES
(1, 1, 1, '2017-07-23'),
(2, 2, 5, '2017-07-24'),
(3, 3, 4, '2017-07-24'),
(4, 2, 2, '2017-07-25'),
(7, 1, 10, '2017-07-25'),
(9, 1, 28, '2017-07-25'),
(10, 1, 14, '2017-07-25'),
(11, 3, 5, '2017-07-25'),
(12, 3, 26, '2017-07-25'),
(13, 3, 26, '2017-07-25'),
(14, 1, 15, '2017-07-25'),
(15, 1, 29, '2017-07-25'),
(16, 1, 11, '2017-07-25'),
(17, 1, 24, '2017-07-25'),
(18, 1, 24, '2017-07-25'),
(19, 7, 14, '2017-07-25'),
(20, 7, 14, '2017-07-26'),
(21, 1, 8, '2017-07-26'),
(22, 1, 8, '2017-07-26'),
(23, 1, 20, '2017-07-26'),
(24, 1, 22, '2017-07-26'),
(25, 1, 14, '2017-07-26');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `last_name` varchar(45) CHARACTER SET utf8 NOT NULL,
  `email` varchar(90) NOT NULL,
  `password` varchar(128) NOT NULL,
  `permision_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `last_name`, `email`, `password`, `permision_id`) VALUES
(1, 'Aner', 'Coric', 'coricaner@yahoo.com', 'alminalmin', 1),
(2, 'almin', 'coric', 'almin@yahoo.com', '1234', 2),
(3, 'amerlina', 'coric', 'amra@gmail.com', '1234', 2),
(4, 'sanjin', 'omerovic', 'sanjin@gmail.com', '1234', 2),
(5, 'sanel', 'coric', 'sanel@hotmail.com', '1234', 2),
(6, 'samir', 'coric', 'samir@hotmail.com', '1234', 2),
(7, 'eman', 'pandur', 'eman@gmail.com', '1234', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cinema_hall`
--
ALTER TABLE `cinema_hall`
  ADD PRIMARY KEY (`id_hall`);

--
-- Indexes for table `movie`
--
ALTER TABLE `movie`
  ADD PRIMARY KEY (`id_movie`);

--
-- Indexes for table `permision`
--
ALTER TABLE `permision`
  ADD PRIMARY KEY (`permision_id`);

--
-- Indexes for table `projection`
--
ALTER TABLE `projection`
  ADD PRIMARY KEY (`id_projection`);

--
-- Indexes for table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`id_reservation`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cinema_hall`
--
ALTER TABLE `cinema_hall`
  MODIFY `id_hall` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `movie`
--
ALTER TABLE `movie`
  MODIFY `id_movie` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `permision`
--
ALTER TABLE `permision`
  MODIFY `permision_id` int(128) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `projection`
--
ALTER TABLE `projection`
  MODIFY `id_projection` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `id_reservation` int(128) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
